import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export enum SubscriptionEnum {
  Free = 'FREE',
  Silver = 'SILVER',
  Gold = 'GOLD',
  Platinum = 'PLATINUM'
}

export type UserType = {
  __typename?: 'UserType';
  id: Scalars['ID'];
  legalId: Scalars['ID'];
  name: Scalars['String'];
  surname: Scalars['String'];
  phoneNum: Scalars['String'];
  email: Scalars['String'];
  dob: Scalars['String'];
  nationality: Scalars['String'];
  subscription: SubscriptionEnum;
  address?: Maybe<AddressType>;
};

export type AddressType = {
  __typename?: 'AddressType';
  street: Scalars['String'];
};

export type CourseType = {
  __typename?: 'CourseType';
  id: Scalars['ID'];
  title: Scalars['String'];
};

export type GameType = {
  __typename?: 'GameType';
  id: Scalars['ID'];
};

export type Query = {
  __typename?: 'Query';
  getUser: UserType;
  getCourses: Array<Maybe<CourseType>>;
};


export type QueryGetUserArgs = {
  id: Scalars['ID'];
};


export type QueryGetCoursesArgs = {
  userId: Scalars['ID'];
};

export type GetUserQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetUserQuery = (
  { __typename?: 'Query' }
  & { getUser: (
    { __typename?: 'UserType' }
    & Pick<UserType, 'id' | 'legalId' | 'name' | 'surname' | 'phoneNum' | 'email' | 'dob' | 'nationality' | 'subscription'>
    & { address?: Maybe<(
      { __typename?: 'AddressType' }
      & Pick<AddressType, 'street'>
    )> }
  ) }
);

export type GetCoursesQueryVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type GetCoursesQuery = (
  { __typename?: 'Query' }
  & { getCourses: Array<Maybe<(
    { __typename?: 'CourseType' }
    & Pick<CourseType, 'id' | 'title'>
  )>> }
);


export const GetUserDocument = gql`
    query getUser($id: ID!) {
  getUser(id: $id) {
    id
    legalId
    name
    surname
    phoneNum
    email
    dob
    nationality
    subscription
    address {
      street
    }
  }
}
    `;

/**
 * __useGetUserQuery__
 *
 * To run a query within a React component, call `useGetUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetUserQuery(baseOptions: Apollo.QueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
        return Apollo.useQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, baseOptions);
      }
export function useGetUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
          return Apollo.useLazyQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, baseOptions);
        }
export type GetUserQueryHookResult = ReturnType<typeof useGetUserQuery>;
export type GetUserLazyQueryHookResult = ReturnType<typeof useGetUserLazyQuery>;
export type GetUserQueryResult = Apollo.QueryResult<GetUserQuery, GetUserQueryVariables>;
export const GetCoursesDocument = gql`
    query getCourses($userId: ID!) {
  getCourses(userId: $userId) {
    id
    title
  }
}
    `;

/**
 * __useGetCoursesQuery__
 *
 * To run a query within a React component, call `useGetCoursesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCoursesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCoursesQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetCoursesQuery(baseOptions: Apollo.QueryHookOptions<GetCoursesQuery, GetCoursesQueryVariables>) {
        return Apollo.useQuery<GetCoursesQuery, GetCoursesQueryVariables>(GetCoursesDocument, baseOptions);
      }
export function useGetCoursesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCoursesQuery, GetCoursesQueryVariables>) {
          return Apollo.useLazyQuery<GetCoursesQuery, GetCoursesQueryVariables>(GetCoursesDocument, baseOptions);
        }
export type GetCoursesQueryHookResult = ReturnType<typeof useGetCoursesQuery>;
export type GetCoursesLazyQueryHookResult = ReturnType<typeof useGetCoursesLazyQuery>;
export type GetCoursesQueryResult = Apollo.QueryResult<GetCoursesQuery, GetCoursesQueryVariables>;