// # PLUGINS IMPORTS //
import React, { ReactNode } from 'react'

// # COMPONENTS IMPORTS //
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  children: ReactNode
}

export default function ApolloWrapper(props: PropsType) {
  const client = new ApolloClient({
    uri: 'http://localhost:3333/graphql',
    cache: new InMemoryCache(),
  })

  return <ApolloProvider client={client}>{props.children}</ApolloProvider>
}
