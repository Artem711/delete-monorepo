module.exports = function (api) {
  api.cache(true)
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            '@lib/data-access': ['./libs/data-access/src/'],
            '@lib/client-config': ['./libs/client-config/src/'],
            /// ///
            '@mobile/utils': ['./apps/mobile/src/shared/utils'],
            '@mobile/components/atoms': [
              './apps/mobile/src/shared/components/atoms',
            ],
            '@mobile/components/molecules': [
              './apps/mobile/src/shared/components/molecules',
            ],
            '@mobile/components/organisms': [
              './apps/mobile/src/shared/components/organisms',
            ],
            '@mobile/components/layout': ['./apps/mobile/src/shared/layout'],
          },
        },
      ],
      'react-native-reanimated/plugin',
    ],
  }
}
