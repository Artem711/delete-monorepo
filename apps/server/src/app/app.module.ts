// # PLUGINS IMPORTS //
import { Module } from '@nestjs/common'

// # COMPONENTS IMPORTS //
import UserModule from './routes/col-user/user.module'
import CourseModule from './routes/gcol-course/course.module'

// # EXTRA IMPORTS //
import { GraphQLConfig } from './config'

/////////////////////////////////////////////////////////////////////////////

@Module({
  imports: [
    UserModule,
    CourseModule,
    //
    GraphQLConfig,
  ],
})
export class AppModule {}
