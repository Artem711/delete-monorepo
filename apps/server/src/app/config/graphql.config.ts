// # PLUGINS IMPORTS //
import { GraphQLModule } from '@nestjs/graphql'

// # COMPONENTS IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export default GraphQLModule.forRoot({
  typePaths: ['./**/*.graphql'],
})
