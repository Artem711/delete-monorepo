// # PLUGINS IMPORTS //
import { HttpService, Injectable } from '@nestjs/common'

// # COMPONENTS IMPORTS //
import { GET_MULTIPLE_COURSES } from '../../constants/graphcms.queries'

// # EXTRA IMPORTS //
import { CourseType } from '@lib/data-access'

/////////////////////////////////////////////////////////////////////////////

@Injectable()
export default class UserService {
  constructor(private httpService: HttpService) {}

  async listMultipleByUserId(userId: string): Promise<CourseType> {
    const {
      data: { data },
    } = await this.httpService
      .post(null, JSON.stringify({ query: GET_MULTIPLE_COURSES }))
      .toPromise()

    return data.courses
  }
}
