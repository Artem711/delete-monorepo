// # PLUGINS IMPORTS //
import { Resolver, Query, Args, ID } from '@nestjs/graphql'

// # COMPONENTS IMPORTS //
import CourseService from './course.service'

// # EXTRA IMPORTS //
import { CourseType } from '@lib/data-access'

/////////////////////////////////////////////////////////////////////////////

@Resolver()
export default class UserResolver {
  constructor(private readonly CourseService: CourseService) {}

  @Query()
  async getCourses(
    @Args('userId', { type: () => ID }) userId: string
  ): Promise<CourseType> {
    return await this.CourseService.listMultipleByUserId(userId)
  }
}
