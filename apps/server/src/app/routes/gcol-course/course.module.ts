// # PLUGINS IMPORTS //
import { HttpModule, Module } from '@nestjs/common'

// # COMPONENTS IMPORTS //
import CourseResolver from './course.resolver'
import CourseService from './course.service'

// # EXTRA IMPORTS //
import { AxiosConfig } from '../../config'

/////////////////////////////////////////////////////////////////////////////

@Module({
  imports: [HttpModule.register(AxiosConfig)],
  providers: [CourseResolver, CourseService],
})
export default class {}
