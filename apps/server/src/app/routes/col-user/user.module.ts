// # PLUGINS IMPORTS //
import { Module } from '@nestjs/common';

// # COMPONENTS IMPORTS //
import UserResolver from './user.resolver';
import UserService from './user.service';

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

@Module({
  providers: [UserResolver, UserService],
})
export default class {}
