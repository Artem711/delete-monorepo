// # PLUGINS IMPORTS //
import { Injectable } from '@nestjs/common';

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import { UserType, SubscriptionEnum } from '@lib/data-access';

/////////////////////////////////////////////////////////////////////////////

@Injectable()
export default class UserService {
  async getOne(id: string): Promise<UserType> {
    return {
      id,
      name: 'Name',
      surname: 'Surname',
      email: 'email',
      phoneNum: 'phoneNum',
      subscription: SubscriptionEnum.Gold,
      address: {
        street: 'Street',
      },
      legalId: 'legal id',
      dob: 'dob',
      nationality: 'Russian',
    };
  }
}
