// # PLUGINS IMPORTS //
import { Resolver, Query, Args, ID } from '@nestjs/graphql';

// # COMPONENTS IMPORTS //
import UserService from './user.service';

// # EXTRA IMPORTS //
import { UserType } from '@lib/data-access';

/////////////////////////////////////////////////////////////////////////////

@Resolver()
export default class UserResolver {
  constructor(private readonly UserService: UserService) {}

  @Query()
  async getUser(@Args('id', { type: () => ID }) id: string): Promise<UserType> {
    return await this.UserService.getOne(id);
  }
}
