export const GET_MULTIPLE_COURSES = `
query courses {
  courses {
    id
    title
  }
}`
