export interface IDimensions {
  width: number
  height: number
  margin?: number
  padding?: number
}
