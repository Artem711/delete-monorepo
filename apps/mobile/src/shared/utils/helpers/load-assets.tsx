// # PLUGINS IMPORTS //
import React, { ReactElement, useCallback, useEffect, useState } from 'react'
import { InitialState, NavigationContainer } from '@react-navigation/native'
import { ThemeProvider } from 'styled-components/native'
import AppLoading from 'expo-app-loading'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { StatusBar } from 'expo-status-bar'
import Constants from 'expo-constants'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ColorSchemeName } from 'react-native-appearance'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import linkingConfig from '../../config/linking.config'
import { theme } from '@mobile/utils'
import { ITheme } from '../theme/colors'

/////////////////////////////////////////////////////////////////////////////
const NAVIGATION_STATE_KEY = `NAVIGATION_STATE_KEY-${Constants.manifest.sdkVersion}`

interface IProps {
  fonts?: FontSource
  assets?: number[]
  children: ReactElement | ReactElement[]
  nativeScheme?: any
}

export default function LoadAssets({
  assets,
  fonts,
  children,
  nativeScheme,
}: IProps) {
  const [isNavigationReady, setIsNavigationReady] = useState(!__DEV__)
  const [initialState, setInitialState] = useState<InitialState | undefined>()
  const ready = useLoadAssets(assets || [], fonts || {})
  const [scheme, setScheme] = useState<ITheme>('light')

  useEffect(() => {
    const restoreState = async () => {
      const schemeResult = await useScheme(nativeScheme)
      // setScheme(schemeResult)

      try {
        const savedStateString = await AsyncStorage.getItem(
          NAVIGATION_STATE_KEY
        )
        const state = savedStateString
          ? JSON.parse(savedStateString)
          : undefined
        setInitialState(state)
      } finally {
        setIsNavigationReady(true)
      }
    }
    if (!isNavigationReady) {
      restoreState()
    }
  }, [isNavigationReady])
  const onStateChange = useCallback(
    (state) =>
      AsyncStorage.setItem(NAVIGATION_STATE_KEY, JSON.stringify(state)),
    []
  )
  if (!ready || !isNavigationReady) {
    return <AppLoading />
  }

  nativeScheme = 'light'

  return (
    <ThemeProvider theme={theme.colors[scheme]}>
      <NavigationContainer
        {...{ onStateChange, initialState }}
        linking={linkingConfig}
        theme={theme.colors[scheme]}
      >
        <StatusBar style="light" />
        {children}
      </NavigationContainer>
    </ThemeProvider>
  )
}

export type FontSource = Parameters<typeof Font.loadAsync>[0]
const usePromiseAll = (
  promises: Promise<void | void[] | Asset[]>[],
  cb: () => void
) =>
  useEffect(() => {
    ;(async () => {
      await Promise.all(promises)
      cb()
    })()
  })

function useLoadAssets(assets: number[], fonts: FontSource): boolean {
  const [ready, setReady] = useState(false)
  usePromiseAll(
    [Font.loadAsync(fonts), ...assets.map((asset) => Asset.loadAsync(asset))],
    () => setReady(true)
  )
  return ready
}

async function useScheme(nativeScheme: ITheme): Promise<ITheme> {
  const scheme = (await AsyncStorage.getItem('scheme')) as ITheme
  return scheme || nativeScheme
}
