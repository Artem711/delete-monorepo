import styled from 'styled-components/native'
export const HeroTitle = styled.Text`
  font-weight: bold;
  font-size: 32px;
`
export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
`
export const Subtitle = styled.Text``
export const Footnote = styled.Text``
export const Caption = styled.Text`
  color: grey;
`
