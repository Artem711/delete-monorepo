import { Theme, DefaultTheme, DarkTheme } from '@react-navigation/native'

export type ITheme = 'dark' | 'light'
interface IThemeObject {
  dark: boolean
  colors: Theme['colors'] & {}
}

const colors: {
  [P in ITheme]: IThemeObject
} = {
  dark: {
    ...DarkTheme,
    dark: true,
    colors: {
      ...DarkTheme.colors,
    },
  },
  light: {
    ...DefaultTheme,
    dark: false,
    colors: {
      ...DefaultTheme.colors,
    },
  },
}

export default colors
