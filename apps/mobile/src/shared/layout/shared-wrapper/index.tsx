// # PLUGINS IMPORTS //
import React, { ComponentClass, FunctionComponent, ReactNode } from 'react'
import { createSharedElementStackNavigator } from 'react-navigation-shared-element'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps<TParamList> {
  screens: Array<{
    title: keyof TParamList
    screen: FunctionComponent | ComponentClass
    sharedElements?: (route: any) => Array<string>
  }>
}

const Stack = createSharedElementStackNavigator()

export default function SharedNavigatorWrapper<
  TParamList extends { [P: string]: object | undefined }
>(props: IProps<TParamList>) {
  return (
    <Stack.Navigator
      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
        cardOverlayEnabled: true,
        cardStyle: { backgroundColor: 'transparent' },
      }}
      mode="modal"
    >
      {props.screens.map((item, index) => (
        <Stack.Screen
          key={index}
          name={item.title as any}
          component={item.screen}
          sharedElements={(route) =>
            item.sharedElements && item.sharedElements(route)
          }
        />
      ))}
    </Stack.Navigator>
  )
}
