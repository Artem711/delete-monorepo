// # PLUGINS IMPORTS //
import React, { ReactNode, useEffect, useRef } from 'react'
import { Animated } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { useSearchContext } from '../search-input/search-provider'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  tabRoute: string
  children: ReactNode
}

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView)
export default function ScrollWrap(props: IProps) {
  const { animation, canJumpToTab } = useSearchContext()
  const ScrollViewRef = useRef(null)

  const onMomentumScrollBegin = () => canJumpToTab(false)
  const onMomentumScrollEnd = () => canJumpToTab(true)

  return (
    <AnimatedScrollView
      ref={ScrollViewRef}
      showsVerticalScrollIndicator={false}
      scrollEventThrottle={1}
      onMomentumScrollBegin={onMomentumScrollBegin}
      onMomentumScrollEnd={onMomentumScrollEnd}
      onScroll={Animated.event(
        [
          {
            nativeEvent: {
              contentOffset: { y: animation && animation.scrollY },
            },
          },
        ],
        { useNativeDriver: true }
      )}
    >
      {props.children}
    </AnimatedScrollView>
  )
}
