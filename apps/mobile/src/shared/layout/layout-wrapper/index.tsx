// # PLUGINS IMPORTS //
import React, { useState } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'

// # COMPONENTS IMPORTS //
import SearchProvider from './search-input/search-provider'
import HeaderBody from './header/header-body'

// # EXTRA IMPORTS //
import { IHeaderIcon, IHeaderTab } from './typings'
import { theme } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

const { HeroTitle } = theme.typography
const { width, height } = Dimensions.get('window')
const initialLayout = {
  width,
  height,
}

interface IProps {
  title: string
  onSearch?: (text: string) => void

  icons: Array<IHeaderIcon>
  tabs?: Array<IHeaderTab>
}

export default function LayoutWrapper(props: IProps) {
  const [index, setIndex] = useState(0)

  return (
    <SearchProvider currentTab={index}>
      <View style={[initialLayout, styles.wrapper]}>
        <HeroTitle>{props.title}</HeroTitle>
        <HeaderBody
          setIndex={setIndex}
          tabs={props.tabs}
          onSearch={props.onSearch}
        />
      </View>
    </SearchProvider>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    width: '92%',
    alignSelf: 'center',
  },
})
