// # PLUGINS IMPORTS //
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { Animated, StyleSheet } from 'react-native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import Tapbar from './header-tapbar'
import SearchInput from '../search-input/search-input'
import ScrollViewWrap from '../utils/scroll-wrap'

import { useSearchContext } from '../search-input/search-provider'
import { IHeaderTab } from '../typings'

/////////////////////////////////////////////////////////////////////////////

const Tab = createMaterialTopTabNavigator()

interface IProps {
  setIndex: (newIndex: number) => void
  tabs?: Array<IHeaderTab>
  onSearch?: (text: string) => void
}

export default function HeaderBody(props: IProps) {
  const { animationFn } = useSearchContext()
  const transformWrapper = animationFn.getTransformWrapper()

  return (
    <Animated.View style={[styles.wrapper, transformWrapper]}>
      {props.onSearch && <SearchInput animation={animationFn} />}
      {props.tabs && (
        <Tab.Navigator
          tabBar={(tabProps) => {
            props.setIndex(tabProps.state.index)
            return <Tapbar {...tabProps} />
          }}
        >
          {props.tabs.map((el) => (
            <Tab.Screen
              name={el.name}
              component={() => (
                <ScrollViewWrap tabRoute={el.name}>
                  {el.component()}
                </ScrollViewWrap>
              )}
            />
          ))}
        </Tab.Navigator>
      )}
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
})
