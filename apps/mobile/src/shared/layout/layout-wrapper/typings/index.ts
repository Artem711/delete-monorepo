import { ReactNode } from 'react'

export interface IHeaderTab {
  name: string
  component: () => ReactNode
}

export interface IHeaderIcon {
  icon: ReactNode
  onPress: () => void
}
