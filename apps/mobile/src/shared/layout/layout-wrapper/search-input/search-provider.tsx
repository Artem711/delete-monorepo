import React, {
  createContext,
  useState,
  useEffect,
  useContext,
  ReactNode,
} from 'react'

import SearchAnimation from './search-animation'
interface IProps {
  children: ReactNode
  currentTab: any
}

const SearchContext = createContext<{
  animation?: any
  animationFn?: any
  addHandlerScroll?: any
  canJumpToTab?: any
}>({})

export const useSearchContext = () => useContext(SearchContext)

export default function SearchProvider(props: IProps) {
  const handlersScroll = {}

  const searchBarAnimation = new SearchAnimation({
    scrollToOffset: (configScroll) => {
      let tab = configScroll.tab ? configScroll.tab : props.currentTab

      let scrollToOffset = handlersScroll[tab]
      scrollToOffset &&
        scrollToOffset(configScroll.offset, configScroll.animated)
    },
  })

  const addHandlerScroll = (tab, handler) => {
    handlersScroll[tab] = handler
  }

  const canJumpToTab = (canJumpToTab: boolean) =>
    setState((prev) => ({ ...prev, canJumpToTab }))

  const [state, setState] = useState({
    currentTab: null,
    canJumpToTab: true,
    contextProvider: {
      animation: searchBarAnimation.animationProps,
      animationFn: searchBarAnimation,
      addHandlerScroll: addHandlerScroll,
      canJumpToTab: canJumpToTab,
    },
  })

  useEffect(() => {
    return () => searchBarAnimation.destroy()
  }, [])

  return (
    <SearchContext.Provider value={state.contextProvider}>
      {props.children}
    </SearchContext.Provider>
  )
}
