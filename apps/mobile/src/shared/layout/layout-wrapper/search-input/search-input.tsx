// # PLUGINS IMPORTS //
import React from 'react'
import { View, StyleSheet, TextInput, Animated } from 'react-native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import { MaterialIcons } from '@expo/vector-icons'

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  animation: any
}

export default function SearchBar(props: IProps) {
  const { animation } = props

  const transformSearchBar = animation.getTransformSearchBar()
  const opacitySearchBar = animation.getOpacitySearchBar()
  const opacityLocationInput = animation.getOpacityLocationInput()

  return (
    <Animated.View style={opacitySearchBar}>
      <View style={styles.searchContainer}>
        <Animated.View
          style={[transformSearchBar, styles.searchInput, opacityLocationInput]}
        >
          <MaterialIcons
            name="search"
            size={22}
            color={'gray'}
            style={styles.searchIcon}
          />
          <TextInput
            style={styles.input}
            placeholder={'Anywhere'}
            autoCorrect={false}
          />
        </Animated.View>
      </View>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  searchContainer: {
    zIndex: 99,
    width: '100%',
    overflow: 'hidden',
    paddingBottom: 16,
  },

  searchInput: {
    height: 40,
    borderRadius: 8,
    marginTop: 8,
    backgroundColor: '#E9EBF0',
  },

  searchIcon: {
    position: 'absolute',
    left: 13,
    top: 10,
  },

  input: {
    marginTop: 11.7,
    marginLeft: 43,
    fontSize: 15,
    color: 'gray',
  },
})
