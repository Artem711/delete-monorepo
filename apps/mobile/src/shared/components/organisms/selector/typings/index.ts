import { ComponentClass, FunctionComponent, ReactNode } from 'react'

export interface ISelectorTab {
  title: string
  component: ComponentClass | FunctionComponent
}
