// # PLUGINS IMPORTS //
import React, { useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import Tapbar from './selector-tapbar'

import { ISelectorTab } from './typings'

/////////////////////////////////////////////////////////////////////////////

const Tab = createMaterialTopTabNavigator()
interface IProps {
  tabs: Array<ISelectorTab>
}

export default function Selector(props: IProps) {
  const [index, setIndex] = useState(0)

  return (
    <View style={styles.wrapper}>
      <Text>dsandjksankjdsa</Text>
      <Tab.Navigator
        tabBar={(tabProps) => {
          setIndex(tabProps.state.index)
          return <Tapbar {...tabProps} />
        }}
      >
        {props.tabs.map((el) => (
          <Tab.Screen name={el.title} component={el.component} />
        ))}
      </Tab.Navigator>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
})
