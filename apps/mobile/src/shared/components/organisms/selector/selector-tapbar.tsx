// # PLUGINS IMPORTS //
import React from 'react'
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
} from 'react-native'
import { Text } from '@mobile/components/atoms'
import { MaterialTopTabBarProps } from '@react-navigation/material-top-tabs'
import Animated, { useAnimatedStyle, withTiming } from 'react-native-reanimated'
import { useTheme } from '@react-navigation/native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const { width } = Dimensions.get('screen')
export default function HeaderTapbar({
  state,
  descriptors,
  navigation,
}: MaterialTopTabBarProps) {
  const { dark } = useTheme()
  const color = dark ? '#19171B' : '#E08AD6'

  const BUTTON_WIDTH = (width - 32) / state.routes.length
  const aStyle = useAnimatedStyle(() => ({
    transform: [
      {
        translateX: withTiming(BUTTON_WIDTH * state.index),
      },
    ],
  }))

  return (
    <View style={styles.wrapper}>
      <View style={StyleSheet.absoluteFill}>
        <Animated.View
          style={[
            styles.bg_selection,
            aStyle,
            {
              width: BUTTON_WIDTH,
              backgroundColor: color,
            },
          ]}
        />
      </View>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key]

        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name

        const isFocused = state.index === index

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          })

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name)
          }
        }

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          })
        }

        return (
          <TouchableWithoutFeedback
            key={index}
            onPress={onPress}
            onLongPress={onLongPress}
          >
            <View style={[styles.label_wrap, { width: BUTTON_WIDTH }]}>
              <Text style={styles.label}>{label}</Text>
            </View>
          </TouchableWithoutFeedback>
        )
      })}
    </View>
  )
}

const SELECTION_WIDTH = width - 32
const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    width: SELECTION_WIDTH,
    alignSelf: 'center',
    marginBottom: 20,
  },

  bg_selection: {
    borderRadius: 4,
    ...StyleSheet.absoluteFillObject,
  },

  label_wrap: {
    padding: 7,
  },

  label: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
})
