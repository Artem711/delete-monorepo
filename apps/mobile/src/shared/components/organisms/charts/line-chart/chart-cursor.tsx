// # PLUGINS IMPORTS //
import React, { FC } from 'react'
import { View, StyleSheet } from 'react-native'
import { Vector, getYForX, clamp, withPause } from 'react-native-redash'
import Animated, {
  useSharedValue,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated'
import { PanGestureHandler } from 'react-native-gesture-handler'
import { useTheme } from '@react-navigation/native'

// # COMPONENTS IMPORTS //
import GraphLine from './chart-line'

// # EXTRA IMPORTS //
import { typings } from '@mobile/utils'
import { IGraphData } from './typings'

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  dimensions: typings.IDimensions
  graphs: Array<IGraphData>
  selectedIndex: Animated.SharedValue<number>
  pointTranslation: Vector<Animated.SharedValue<number>>
}

const GraphCursor: FC<PropsType> = ({
  selectedIndex,
  pointTranslation,
  graphs,
  dimensions,
}) => {
  const { dark } = useTheme()
  const isActive = useSharedValue(false)

  const onGestureEvent = useAnimatedGestureHandler({
    onStart: () => {
      isActive.value = true
    },
    onActive: ({ x }) => {
      pointTranslation.x.value = clamp(x, 0, dimensions.width)
      if (
        pointTranslation.x.value > 0 &&
        pointTranslation.x.value < dimensions.width
      ) {
        pointTranslation.y.value = getYForX(
          graphs[selectedIndex.value].data.path,
          pointTranslation.x.value
        )
      }
    },
    onEnd: () => {
      isActive.value = false
    },
  })

  const style = useAnimatedStyle(() => {
    const translateX = pointTranslation.x.value - CURSOR_SIZE / 2
    const translateY = pointTranslation.y.value - CURSOR_SIZE / 2

    return {
      transform: [
        { translateX },
        { translateY },
        { scale: withSpring(isActive.value ? 1 : 0) },
      ],
    }
  })

  const verticalStyle = useAnimatedStyle(() => ({
    opacity: withSpring(isActive.value ? 1 : 0),
    transform: [{ translateX: pointTranslation.x.value }],
  }))

  return (
    <View style={StyleSheet.absoluteFill}>
      <PanGestureHandler onGestureEvent={onGestureEvent}>
        <Animated.View style={StyleSheet.absoluteFill}>
          <Animated.View
            style={[
              styles.wrapper,
              style,
              { backgroundColor: dark ? 'white' : 'rgba(0, 0, 0, 0.1)' },
            ]}
          >
            <View style={styles.body} />
          </Animated.View>
          <Animated.View style={[StyleSheet.absoluteFill, verticalStyle]}>
            <GraphLine x={0} y={dimensions.width} />
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
    </View>
  )
}

const CURSOR_SIZE = 40
const styles = StyleSheet.create({
  wrapper: {
    width: CURSOR_SIZE,
    height: CURSOR_SIZE,
    borderRadius: CURSOR_SIZE / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  body: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    backgroundColor: 'black',
  },
})

export default GraphCursor
