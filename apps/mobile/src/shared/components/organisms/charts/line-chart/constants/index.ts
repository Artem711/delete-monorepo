// PLUGINS IMPORTS //
// @ts-ignore
import data from './data.json'
import { IPrices, IDataPoints, IGraphPath, IGraphData } from '../typings'
import { parse } from 'react-native-redash'
import * as d3 from 'd3'

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //
import { typings } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

const POINTS = 60
const values = data.data.prices as IPrices

const buildGraph = (
  datapoints: IDataPoints,
  label: string,
  dimensions: typings.IDimensions
): IGraphPath => {
  // 1) Format data
  const priceList = datapoints.prices.slice(0, POINTS)
  const formattedValues = priceList.map(
    (price) => [parseFloat(price[0]), price[1]] as [number, number]
  )

  const xAccessor = (d) => d[1]
  const yAccessor = (d) => d[0]

  // 2) Define scaleX and scaleY (using max and min values of each dataset)
  // scale X
  const scaleX = d3
    .scaleTime()
    .domain(d3.extent(formattedValues, xAccessor) as any)
    .range([0, dimensions.width])

  // scale Y
  const priceExtent = d3.extent(formattedValues, yAccessor)
  const scaleY = d3
    .scaleLinear()
    .domain(priceExtent)
    .range([dimensions.height, 0])

  // 3) Define path
  const path = parse(
    d3
      .line()
      .x(([, x]) => scaleX(x))
      .y(([y]) => scaleY(y))
      .curve(d3.curveBasis)(formattedValues) as string
  )

  return {
    label,
    minPrice: priceExtent[0],
    maxPrice: priceExtent[1],
    percentChange: datapoints.percent_change,
    path,
  }
}

export function renderGraphs(
  dimensions: typings.IDimensions
): Array<IGraphData> {
  return [
    {
      label: '1H',
      value: 0,
      data: buildGraph(values.hour, 'Last Hour', dimensions),
    },
    {
      label: '1D',
      value: 1,
      data: buildGraph(values.day, 'Today', dimensions),
    },
    {
      label: '1M',
      value: 2,
      data: buildGraph(values.month, 'Last Month', dimensions),
    },
    {
      label: '1Y',
      value: 3,
      data: buildGraph(values.year, 'This Year', dimensions),
    },
    {
      label: 'all',
      value: 4,
      data: buildGraph(values.all, 'All time', dimensions),
    },
  ]
}
