// PLUGINS IMPORTS //
import React, { FC } from 'react'
import { View, TouchableWithoutFeedback, StyleSheet } from 'react-native'
import Animated, { useAnimatedStyle, withTiming } from 'react-native-reanimated'
import { Text } from '@mobile/components/atoms'

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //
import { IDimensions, IGraphData } from './typings'

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  graphs: Array<IGraphData>
  dimensions: IDimensions
  handleSwitchGraph: (index: number) => void
  selectedIndex: Animated.SharedValue<number>
}

const color = '#CACACA'
const GraphSelector: FC<PropsType> = ({
  dimensions,
  graphs,
  selectedIndex,
  handleSwitchGraph,
}) => {
  const SELECTION_WIDTH = dimensions.width - 32
  const BUTTON_WIDTH = (dimensions.width - 32) / graphs.length

  const aStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: withTiming(BUTTON_WIDTH * selectedIndex.value) }],
  }))

  return (
    <View style={[styles.wrapper, { width: SELECTION_WIDTH }]}>
      <View style={StyleSheet.absoluteFill}>
        <Animated.View
          style={[styles.bg_selection, { width: BUTTON_WIDTH }, aStyle]}
        />
      </View>
      {graphs.map((graph, index) => {
        return (
          <TouchableWithoutFeedback
            key={graph.label}
            onPress={() => handleSwitchGraph(index)}
          >
            <View style={[styles.label_wrap, { width: BUTTON_WIDTH }]}>
              <Text style={[styles.label, {}]}>{graph.label}</Text>
            </View>
          </TouchableWithoutFeedback>
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',

    alignSelf: 'center',
    marginBottom: 15,
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  bg_selection: {
    backgroundColor: color,
    ...StyleSheet.absoluteFillObject,
    borderRadius: 8,
  },

  label_wrap: {
    padding: 16,
    color: 'white',
  },

  label: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
})

export default GraphSelector
