// PLUGINS IMPORTS //
import React from 'react'
import { View } from 'react-native'
import {
  useSharedValue,
  withTiming,
  useAnimatedProps,
} from 'react-native-reanimated'
import { mixPath, useVector } from 'react-native-redash'

// COMPONENTS IMPORTS //
import ChartHeader from './chart-header'
import ChartCursor from './chart-cursor'
import ChartSelector from './chart-selector'
import ChartSVG from './chart-svg'

// EXTRA IMPORTS //
import { renderGraphs } from './constants'
import { typings } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  dimensions: typings.IDimensions
}

const LineGraph = (props: IProps) => {
  const pointTranslation = useVector()
  const transition = useSharedValue(0)
  const previousIndex = useSharedValue(0)
  const currentIndex = useSharedValue(0)

  const graphs = renderGraphs(props.dimensions)
  const handleSwitchGraph = (index: number) => {
    previousIndex.value = currentIndex.value
    transition.value = 0
    currentIndex.value = index
    transition.value = withTiming(1)
  }

  const GraphAnimatedProps = useAnimatedProps(() => {
    const currentPath = graphs[currentIndex.value].data.path
    const previousPath = graphs[previousIndex.value].data.path

    return {
      d: mixPath(transition.value, previousPath, currentPath),
    }
  })

  const GraphAnimatedProps2 = useAnimatedProps(() => {
    const currentPath = graphs[currentIndex.value].data.path
    const previousPath = graphs[previousIndex.value].data.path

    return {
      d: `${mixPath(transition.value, previousPath, currentPath)} L ${
        props.dimensions.width / 30
      } ${props.dimensions.height} L 900 ${props.dimensions.height}`,
    }
  })

  return (
    <View style={props.dimensions}>
      {/* <View> */}
      {/* <ChartHeader
          graphs={graphs}
          dimensions={props.dimensions}
          selectedIndex={currentIndex}
          pointTranslation={pointTranslation}
        /> */}
      {/* <View> */}
      <ChartSVG
        dimensions={props.dimensions}
        aProps={GraphAnimatedProps}
        aProps2={GraphAnimatedProps2}
      />
      <ChartCursor
        graphs={graphs}
        dimensions={props.dimensions}
        selectedIndex={currentIndex}
        pointTranslation={pointTranslation}
      />
      {/* </View> */}
      {/* </View> */}
      {/* <View>
        <ChartSelector
          graphs={graphs}
          dimensions={props.dimensions}
          selectedIndex={currentIndex}
          handleSwitchGraph={handleSwitchGraph}
        />
      </View> */}
    </View>
  )
}

export default LineGraph
