// PLUGINS IMPORTS //
import React, { FC } from 'react'
import Svg, { Defs, LinearGradient, Path, Stop } from 'react-native-svg'
import Animated from 'react-native-reanimated'

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //
import { typings } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  dimensions: typings.IDimensions

  aProps: { d: string }
  aProps2: { d: string }
}

const AnimatedPath = Animated.createAnimatedComponent(Path)
const GraphSVG: FC<PropsType> = (props) => {
  return (
    <Svg width={props.dimensions.width} height={props.dimensions.height}>
      <Defs>
        <LinearGradient x1={'50%'} x2="50%" y2="100%" id="gradient">
          <Stop stopColor="#CDE3F8" offset="0%" />
          <Stop stopColor="#eef6fd" offset="80%" />
          <Stop stopColor="#FEFFFF" offset="100%" />
        </LinearGradient>
      </Defs>
      <AnimatedPath animatedProps={props.aProps2} fill="url(#gradient)" />

      <AnimatedPath
        animatedProps={props.aProps}
        fill={'transparent'}
        stroke={'#1A6FDD'}
        strokeWidth={3}
      />
    </Svg>
  )
}

export default GraphSVG
