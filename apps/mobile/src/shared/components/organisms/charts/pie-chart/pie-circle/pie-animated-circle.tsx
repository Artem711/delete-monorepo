// # PLUGINS IMPORTS //
import React, { memo, ReactNode, useEffect } from 'react'
import { Circle, CircleProps } from 'react-native-svg'
import Animated, {
  useAnimatedProps,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IAnimatedCircle {
  native: CircleProps

  animation: {
    circumference: number
    amount: number

    duration?: number
    delay?: number
    max: number
  }
}

const AnimatedCircle = Animated.createAnimatedComponent(Circle)
export default function AnimatedPieCircle(props: IAnimatedCircle) {
  const {
    native,
    animation: { amount, circumference, duration = 500, delay = 0, max },
  } = props

  const offset = useSharedValue(circumference)

  useEffect(() => {
    const maxPerc = (100 * amount) / max
    const strokeDashoffset = circumference - (circumference * maxPerc) / 100

    offset.value = strokeDashoffset + 20
  }, [])

  const animatedProps = useAnimatedProps(() => ({
    strokeDashoffset: withSpring(offset.value, { mass: 2 }),
  }))

  return (
    <AnimatedCircle
      {...native}
      strokeDasharray={circumference}
      strokeDashoffset={circumference}
      // strokeLinecap={'round'}
      animatedProps={animatedProps}
    />
  )
}
