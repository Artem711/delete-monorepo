import React, { memo } from 'react'
import { View, Dimensions } from 'react-native'
import { Text } from '@mobile/components/atoms'
import Svg, { G, Path } from 'react-native-svg'
import styled from 'styled-components/native'
import * as d3 from 'd3'

// # COMPONENTS IMPORTS //
import PieCircle from './pie-circle'

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  data: Array<{ title: string; amount: number; color: string }>
  config: {
    max: number
    radius?: number
    strokeWidth?: number
    backgroundColor?: string
    textColor?: string
  }

  animation?: {
    duration?: number
    delay?: number
  }
}

export default function PieChart(props: IProps) {
  const { data, animation, config } = props
  const {
    max = 100,
    radius = 40,
    strokeWidth = 10,
    backgroundColor = 'tomato',
  } = config

  const halfCircle = radius + strokeWidth
  const circumference = 2 * Math.PI * radius

  const arcs = d3
    .pie()(data.map((el) => el.amount))
    .map((item) => ({
      ...item,
      startAngle: item.startAngle * (180 / Math.PI),
      endAngle: item.endAngle * (180 / Math.PI),
    }))

  return (
    <View>
      <Svg
        width={radius * 2}
        height={radius * 2}
        viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}
      >
        <G rotation={-90} origin={`${halfCircle}, ${halfCircle}`}>
          <PieCircle
            radius={radius}
            color={backgroundColor}
            strokeOpacity={0.4}
            strokeWidth={strokeWidth}
          />

          {arcs.map((item, index) => (
            <PieCircle
              radius={radius}
              color={data[index].color}
              strokeWidth={strokeWidth}
              animation={{
                circumference,
                amount: item.data as any,
                max,
                ...animation,
              }}
              native={{
                rotation: item.startAngle,
                origin: `${halfCircle}, ${halfCircle}`,
              }}
            >
              <Text>dsadsa</Text>
            </PieCircle>
          ))}
        </G>
      </Svg>
    </View>
  )
}
