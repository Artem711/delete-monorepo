// # PLUGINS IMPORTS //
import React, { memo, ReactNode } from 'react'

// # COMPONENTS IMPORTS //
import Circle from './pie-circle'
import AnimatedCircle from './pie-animated-circle'
import { CircleProps } from 'react-native-svg'

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface ICircle {
  children?: ReactNode
  radius: number
  color: string
  strokeWidth: number
  strokeOpacity?: number
  animation?: {
    circumference: number
    amount: number
    duration?: number
    delay?: number
    max: number
  }
  native?: CircleProps
}

function PieCircle(props: ICircle) {
  const native = {
    cx: '50%',
    cy: '50%',
    r: props.radius,
    stroke: props.color,
    strokeWidth: props.strokeWidth,
    strokeOpacity: props.strokeOpacity,
    fill: 'transparent',
    ...props.native,
  }

  if (props.animation) {
    return <AnimatedCircle native={native} animation={props.animation} />
  } else {
    return <Circle {...native}>{props.children}</Circle>
  }
}

///////////////////////////////////////////

export default memo(PieCircle)
