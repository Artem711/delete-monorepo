// # PLUGINS IMPORTS //
import React, { memo, ReactNode } from 'react'
import { Circle, CircleProps } from 'react-native-svg'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps extends CircleProps {
  children: ReactNode
}

function PieCircle(props: IProps) {
  return (
    <>
      <Circle {...props} />
      {props.children}
    </>
  )
}

///////////////////////////////////////////

export default memo(PieCircle)
