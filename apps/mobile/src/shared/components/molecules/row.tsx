// # PLUGINS IMPORTS //
import React, { ReactNode } from 'react'
import { View } from 'react-native'
import { Text } from '@mobile/components/atoms'
import styled from 'styled-components/native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  image: ReactNode
  title: string
  color: string
  prices: { num: number; perc: number; positive: boolean }
  data: { shares: number; sharePrice: number }
}

export default function Row(props: IProps) {
  return (
    <Wrapper>
      <ImageWrap>{props.image}</ImageWrap>
      <View>
        <Text>{props.title}</Text>
        <Text>
          {props.data.shares} Shares | {props.data.sharePrice} $ per share
        </Text>
      </View>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  height: 60;
`

const ImageWrap = styled.View`
  margin-right: 10;
`
