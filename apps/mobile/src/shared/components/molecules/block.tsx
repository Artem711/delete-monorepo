// # PLUGINS IMPORTS //
import React, { ReactNode } from 'react'
import { ViewStyle } from 'react-native'
import { useTheme } from '@react-navigation/native'
import styled from 'styled-components/native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import { typings } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

export type IProps = {
  children: ReactNode
  style?: ViewStyle
  dimensions?: typings.IDimensions
}

export default function Block(props: IProps) {
  const { dark } = useTheme()

  return (
    <SWrapper
      style={[
        {
          height: props.dimensions ? props.dimensions.height : undefined,
          width: props.dimensions ? props.dimensions.width : undefined,
          backgroundColor: dark ? '#171717' : 'vanilla',
          shadowOffset: { width: 0, height: 3 },
        },
        props.style,
      ]}
    >
      {props.children}
    </SWrapper>
  )
}

///////////////////////////////////////////

const SWrapper = styled.View`
  border-radius: 6px;
`
