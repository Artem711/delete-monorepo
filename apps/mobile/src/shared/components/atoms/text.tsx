// # PLUGINS IMPORTS //
import React from 'react'
import { Text as DefaultText } from 'react-native'
import { useTheme } from '@react-navigation/native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export type IProps = { fontSize?: number } & DefaultText['props']
export default function Text(props: IProps) {
  const { fontSize, style, ...otherProps } = props
  const { dark } = useTheme()

  return (
    <DefaultText
      style={[{ fontSize, color: dark ? 'white' : 'black' }, style]}
      {...otherProps}
    />
  )
}
