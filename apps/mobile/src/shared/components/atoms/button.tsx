// # PLUGINS IMPORTS //
import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Text } from '@mobile/components/atoms'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export type IProps = {
  text: string
  onPress?: () => void
}

export default function Button(props: IProps) {
  return (
    <TouchableOpacity
      style={{ backgroundColor: 'black' }}
      onPress={props.onPress}
    >
      <Text>{props.text}</Text>
    </TouchableOpacity>
  )
}
