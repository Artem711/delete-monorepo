// # PLUGINS IMPORTS //
import React from 'react'
import { createSharedElementStackNavigator } from 'react-navigation-shared-element'

// # COMPONENTS IMPORTS //
import BottomTabNavigator from './BottomTab.navigator'
import OrderScreen from '../screens/order.sreen'

// # EXTRA IMPORTS //
import { IRootStackParams } from './typings'

/////////////////////////////////////////////////////////////////////////////

const Stack = createSharedElementStackNavigator<IRootStackParams>()
export default function RootStackNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
        cardOverlayEnabled: true,
        cardStyle: { backgroundColor: 'transparent' },
      }}
      mode="modal"
    >
      <Stack.Screen name="OrderScreen" component={OrderScreen} />
      {/* <Stack.Screen name="Root" component={BottomTabNavigator} /> */}
    </Stack.Navigator>
  )
}
