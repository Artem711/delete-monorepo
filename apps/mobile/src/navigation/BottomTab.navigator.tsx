// # PLUGINS IMPORTS //
import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'

// # COMPONENTS IMPORTS //
import * as TabScreens from '../screens/tabs'

// # EXTRA IMPORTS //
import { IBottomTabParams } from './typings'
import { Ionicons } from '@expo/vector-icons'
import SharedNavigatorWrapper from '../shared/layout/shared-wrapper'
import { IStory } from '../screens/simple/story.screen/story.screen'
import { StoryScreen } from '../screens/simple/story.screen'

/////////////////////////////////////////////////////////////////////////////

const Tab = createMaterialBottomTabNavigator<IBottomTabParams>()
export default function BottomTabNavigator() {
  return (
    <Tab.Navigator initialRouteName="OverviewScreen">
      <Tab.Screen
        name="OverviewScreen"
        component={TabScreens.OverviewScreen}
        options={{
          title: 'Overview',
          tabBarIcon: ({ color }) => (
            <Ionicons size={22} name="ios-code" color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="BotScreen"
        component={TabScreens.BotScreen}
        options={{
          title: 'Bot',
          tabBarIcon: ({ color }) => (
            <Ionicons size={22} name="ios-code" color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="DiscoverScreen"
        component={TabScreens.DiscoverScreen}
        options={{
          title: 'Disocever',
          tabBarIcon: ({ color }) => (
            <Ionicons size={22} name="ios-code" color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="AcademyScreen"
        component={TabScreens.AcademyScreen}
        options={{
          title: 'Academy',
          tabBarIcon: ({ color }) => (
            <Ionicons size={22} name="ios-code" color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileScreenStack"
        component={ProfileScreenStack}
        options={{
          title: 'Profile',
          tabBarIcon: ({ color }) => (
            <Ionicons size={22} name="ios-code" color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  )
}

function ProfileScreenStack() {
  return (
    <SharedNavigatorWrapper<{
      ProfileScreen: undefined
      StoryScreen: { story: IStory }
    }>
      screens={[
        { title: 'ProfileScreen', screen: TabScreens.ProfileScreen },
        {
          title: 'StoryScreen',
          screen: StoryScreen as any,
          sharedElements: (route) => [route.params.story.id],
        },
      ]}
    />
  )
}
