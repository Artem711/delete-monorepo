// # PLUGINS IMPORTS //
import React from 'react'
import { AppearanceProvider, useColorScheme } from 'react-native-appearance'

// # COMPONENTS IMPORTS //
import RootStackNavigator from './RootStack.navigator'
import { assets as profileAssets } from '../screens/tabs/profile.screen'

// # EXTRA IMPORTS //
import { LoadAssets } from '../shared/utils/helpers'

/////////////////////////////////////////////////////////////////////////////

const assets = [...profileAssets]

export default function Navigation() {
  const scheme = useColorScheme()

  return (
    <AppearanceProvider>
      <LoadAssets assets={assets} nativeScheme={scheme}>
        <RootStackNavigator />
      </LoadAssets>
    </AppearanceProvider>
  )
}
