import { IStory } from '../../screens/simple/story.screen/story.screen'

export type IRootStackParams = {
  Root: undefined
  StoryScreen: { story: IStory }
  OrderScreen: undefined
}

export type IBottomTabParams = {
  OverviewScreen: undefined
  BotScreen: undefined
  DiscoverScreen: undefined
  AcademyScreen: undefined
  ProfileScreenStack: undefined
}

export type IStoryStack = {}
