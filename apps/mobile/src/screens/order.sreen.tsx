// # PLUGINS IMPORTS //
import React from 'react'
import { View, StyleSheet, Text, Dimensions } from 'react-native'
import { PanGestureHandler } from 'react-native-gesture-handler'
import Animated, {
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withDecay,
  withTiming,
} from 'react-native-reanimated'
import { clamp } from 'react-native-redash'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import { MaterialIcons } from '@expo/vector-icons'
import { off } from 'process'

/////////////////////////////////////////////////////////////////////////////

const { height } = Dimensions.get('screen')
const CARD_HEIGHT = height
export default function OrderScreen() {
  const translateY = useSharedValue(0)

  const boundY = height - CARD_HEIGHT
  const offset = 100
  const onGestureEvent = useAnimatedGestureHandler({
    // Starting from previous position
    onStart: (event, ctx: any) => {
      ctx.offsetY = translateY.value
    },
    // Moving card around (setting translation values)
    onActive: ({ translationY }, ctx) => {
      translateY.value = clamp(
        translationY + ctx.offsetY,
        -(height - offset),
        -offset
      )
    },
    // onEnd: (event) => {
    //   translateY.value = withDecay({
    //     velocity: event.velocityY,
    //     // clamp: [0, boundY],
    //   })
    // },
  })

  const animatedStyle = useAnimatedStyle(() => {
    const borderRadius = clamp(Math.abs(translateY.value / 1.4), 0, 40)
    return {
      transform: [{ translateY: translateY.value }],
      borderBottomRightRadius: borderRadius,
      borderBottomLeftRadius: borderRadius,
    }
  }, [translateY])

  const textStyle = useAnimatedStyle(() => {
    return {
      opacity: Math.abs(translateY.value),
    }
  })

  return (
    <View style={styles.wrapper}>
      <PanGestureHandler onGestureEvent={onGestureEvent}>
        <Animated.View
          style={[styles.container, { height: CARD_HEIGHT }, animatedStyle]}
        >
          <Text>dsadsa</Text>
          <MaterialIcons name="keyboard-arrow-up" size={24} color="black" />
          <Animated.View
            style={[
              { position: 'absolute', bottom: -45, alignSelf: 'center' },
              textStyle,
            ]}
          >
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>
              Swipe Up To Submit
            </Text>
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#2EB62D',
  },

  container: {
    backgroundColor: 'white',
  },
})
