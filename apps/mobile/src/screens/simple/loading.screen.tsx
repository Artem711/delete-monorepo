// # PLUGINS IMPORTS //
import React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export default function LoadingScreen() {
  return (
    <SWrapper>
      <ActivityIndicator color={'black'} size={'large'} />
    </SWrapper>
  )
}

const SWrapper = styled.View`
  flex: 1;
  justify-content: center;
`
