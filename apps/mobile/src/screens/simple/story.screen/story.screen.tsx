// # PLUGINS IMPORTS //
import React from 'react'
import { StyleSheet, Dimensions, View } from 'react-native'
import { useRoute } from '@react-navigation/native'
import { PanGestureHandler } from 'react-native-gesture-handler'
import { SharedElement } from 'react-navigation-shared-element'
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated'
import { Video } from 'expo-av'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //
import { snapPoint, useVector } from 'react-native-redash'

/////////////////////////////////////////////////////////////////////////////

export interface IStory {
  id: string
  source: number
  user: string
  avatar: number
  video?: number
}

interface IProps {
  navigation: any
}

const { height } = Dimensions.get('screen')
const AnimatedVideo = Animated.createAnimatedComponent(Video)
export default function StoryScreen({ navigation }: IProps) {
  const {
    params: { story },
  } = useRoute() as any

  const isGestureActive = useSharedValue(false)
  const translate = useVector()
  const onGestureEvent = useAnimatedGestureHandler({
    onStart: () => {
      isGestureActive.value = true
    },
    onActive: ({ translationX, translationY }) => {
      translate.x.value = translationX
      translate.y.value = translationY
    },
    onEnd: ({ velocityX, velocityY }) => {
      const shouldGoBack =
        snapPoint(translate.y.value, velocityY, [0, height]) === height

      if (shouldGoBack) {
        runOnJS(navigation.goBack)()
      } else {
        translate.x.value = withSpring(0, { velocity: velocityX })
        translate.y.value = withSpring(0, { velocity: velocityY })
      }

      isGestureActive.value = false
    },
  })

  const borderStyle = useAnimatedStyle(() => ({
    borderRadius: withTiming(isGestureActive.value ? 24 : 0),
  }))
  const style = useAnimatedStyle(() => {
    const scale = interpolate(
      translate.y.value,
      [0, height],
      [1, 0.5],
      Extrapolate.CLAMP
    )

    return {
      transform: [
        { translateX: translate.x.value * scale },
        { translateY: translate.y.value * scale },
        { scale },
      ],
    }
  })

  const backdropStyles = useAnimatedStyle(() => ({
    backgroundColor: `rgba(0, 0, 0, ${
      isGestureActive.value
        ? 1 - translate.y.value / 1000
        : withTiming(0, { duration: 9999 })
    })`,
  }))

  return (
    <View style={{ flex: 1 }}>
      <Animated.View style={[StyleSheet.absoluteFill, backdropStyles]} />
      <PanGestureHandler onGestureEvent={onGestureEvent}>
        <Animated.View style={[style, { flex: 1 }]}>
          <SharedElement id={story.id} style={{ flex: 1 }}>
            {!story.video && (
              <Animated.Image
                source={story.source}
                style={[
                  borderStyle,
                  {
                    ...StyleSheet.absoluteFillObject,
                    width: undefined,
                    height: undefined,
                  },
                ]}
              />
            )}
            {story.video && (
              <AnimatedVideo
                source={story.video}
                rate={1.0}
                isMuted={false}
                resizeMode="cover"
                shouldPlay
                isLooping
                style={[StyleSheet.absoluteFill, borderStyle]}
              />
            )}
          </SharedElement>
        </Animated.View>
      </PanGestureHandler>
    </View>
  )
}
