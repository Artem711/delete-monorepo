// # PLUGINS IMPORTS //
import React, { useState } from 'react'
import { useFocusEffect, useNavigation } from '@react-navigation/native'
import { SharedElement } from 'react-navigation-shared-element'
import { View, Image, StyleSheet, Dimensions, Pressable } from 'react-native'
import { IStory } from './story.screen'

// # COMPONENTS IMPORTS //

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps {
  story: IStory
}

const margin = 16
const borderRadius = 5
const width = Dimensions.get('window').width / 2 - margin * 2
export default function StoryScreen(props: IProps) {
  const [opacity, setOpacity] = useState(1)
  const navigation = useNavigation()
  useFocusEffect(() => {
    if (navigation.isFocused()) {
      setOpacity(1)
    }
  })

  return (
    <Pressable
      style={({ pressed }) => ({ opacity: pressed ? 0.5 : 1 })}
      onPress={() => {
        navigation.navigate('StoryScreen', { story: props.story })
        setOpacity(0)
      }}
    >
      <View style={[styles.wrapper, { opacity }]}>
        <SharedElement id={props.story.id} style={{ flex: 1 }}>
          <Image source={props.story.source} style={styles.image} />
        </SharedElement>
      </View>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    width,
    height: width * 1.77,
    marginTop: 16,
    borderRadius,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
    resizeMode: 'cover',
    borderRadius,
  },
})
