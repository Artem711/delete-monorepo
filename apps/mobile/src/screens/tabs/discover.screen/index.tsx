// # PLUGINS IMPORTS //
import React, { memo } from 'react'
import { View, ScrollView, Dimensions } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styled from 'styled-components/native'

// # COMPONENTS IMPORTS //
import { Block } from '@mobile/components/atoms'
import { Charts } from '@mobile/components/organisms'
import { theme } from '@mobile/utils'

const {
  typography: { Title, Footnote, Caption },
} = theme

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const { width, height } = Dimensions.get('screen')
function DiscoverScreen() {
  const data = Array(8).fill({
    title: 'Общая сумма расходов',
    balance: 78,
    subtitles: [
      { title: 'Текущие расходы', value: 78 },
      { title: 'Текущие расходы', value: 78 },
    ],
  })

  const blockDimensions = {
    width: width * 0.94,
    height: height / 2.6,
    margin: 10,
    padding: 10,
  }
  return (
    <SafeAreaView>
      <ScrollView
        horizontal
        pagingEnabled
        decelerationRate={0}
        snapToAlignment="center"
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        snapToInterval={blockDimensions.width + blockDimensions.margin * 2}
      >
        {data.map((el) => (
          <Block
            style={{
              marginHorizontal: blockDimensions.margin,
              paddingHorizontal: blockDimensions.padding,
              paddingVertical: blockDimensions.padding,
              paddingBottom: blockDimensions.padding + 10,
            }}
            dimensions={blockDimensions}
          >
            <Title>{el.balance} $</Title>
            <Caption>{el.title}</Caption>
            <View style={{ justifyContent: 'space-between', flex: 1 }}>
              <Charts.LineChart
                dimensions={{
                  width: blockDimensions.width - blockDimensions.padding * 2,
                  height: blockDimensions.height * 0.6,
                }}
              />
              <SRow>
                {el.subtitles.map((item: any) => (
                  <SColumn>
                    <Footnote>{item.title}</Footnote>
                    <Caption>{item.value} $</Caption>
                  </SColumn>
                ))}
              </SRow>
            </View>
          </Block>
        ))}
      </ScrollView>
    </SafeAreaView>
  )
}

///////////////////////////////////////////

const SRow = styled.View`
  flex-direction: row;
`

const SColumn = styled.View`
  flex-direction: column;
  margin-right: 20;
`

export default memo(DiscoverScreen)
