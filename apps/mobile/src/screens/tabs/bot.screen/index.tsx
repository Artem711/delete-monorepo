// # PLUGINS IMPORTS //
import React from 'react'
import { View, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

// # COMPONENTS IMPORTS //
import { LayoutWrapper } from '@mobile/components/layout'
import { Text, Row } from '@mobile/components/atoms'

// # EXTRA IMPORTS //
import { MaterialIcons } from '@expo/vector-icons'

/////////////////////////////////////////////////////////////////////////////

export default function BotScreen() {
  const dataSource = Array(20)
    .fill('')
    .map((_, index) => ({ id: index }))

  return (
    <SafeAreaView>
      <LayoutWrapper
        tabs={[
          {
            name: 'Name',
            component: () => (
              <>
                {dataSource.map((el) => (
                  <Row
                    image={
                      <MaterialIcons
                        name="account-circle"
                        size={24}
                        color="black"
                      />
                    }
                    title={'Apple'}
                    color={'white'}
                    prices={{ num: 97.54, perc: 2.02, positive: true }}
                    data={{ shares: 19, sharePrice: 26.93 }}
                  />
                ))}
              </>
            ),
          },
          {
            name: 'Nasme',
            component: () => (
              <View>
                <Text>Name</Text>
              </View>
            ),
          },
          {
            name: 'Naasme',
            component: () => (
              <View>
                <Text>Name</Text>
              </View>
            ),
          },
        ]}
        title={'Title'}
        icons={[]}
        onSearch={(text) => console.log(text)}
      />
    </SafeAreaView>
  )
}
