// # PLUGINS IMPORTS //
import React, { memo } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

// # COMPONENTS IMPORTS //
import Header from './overview-header'
import Body from './overview-body'

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export default function OverviewScreen() {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header />
      <Body />
    </SafeAreaView>
  )
}

///////////////////////////////////////////
