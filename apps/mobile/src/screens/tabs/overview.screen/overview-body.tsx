// # PLUGINS IMPORTS //
import React from 'react'
import { View, Text } from 'react-native'

// # COMPONENTS IMPORTS //
import { Block } from '@mobile/components/atoms'
import { Selector } from '@mobile/components/organisms'

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IProps {}

export default function OverviewBody(props: IProps) {
  return (
    <Block style={{ flex: 1 }}>
      <Selector
        tabs={[
          {
            title: 'By product',
            component: () => (
              <View>
                <Text>lkdfsankjfsandkjsnajkdsa</Text>
                <Text>lkdfsankjfsandkjsnajkdsa</Text>
              </View>
            ),
          },
          { title: 'By category', component: () => <View></View> },
          { title: 'By industry', component: () => <View></View> },
        ]}
      />
    </Block>
  )
}

///////////////////////////////////////////
