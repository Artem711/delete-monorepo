// # PLUGINS IMPORTS //
import React, { memo } from 'react'
import { View, ScrollView, Dimensions } from 'react-native'
import { sum } from 'd3'

// # COMPONENTS IMPORTS //
import { Block } from '@mobile/components/atoms'
import { Charts } from '@mobile/components/organisms'
import { theme } from '@mobile/utils'

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const { width, height } = Dimensions.get('screen')
function OverviewHeader() {
  const data = Array(8).fill({
    title: 'Обща сумма расходов',
    balance: 78,
    subtitles: [
      { title: 'Текущие расходы', value: 78 },
      { title: 'Текущие расходы', value: 78 },
    ],
  })

  const blockDimensions = {
    width: width * 0.94,
    height: height / 2.6,
    margin: 10,
    padding: 10,
  }

  const circleData = [
    { title: 'Something', amount: 22, color: 'tomato' },
    { title: 'Something', amount: 11, color: 'green' },
    { title: 'Something', amount: 2, color: 'cyan' },
    { title: 'Something', amount: 3, color: 'brown' },
  ]

  return (
    <View>
      <ScrollView
        style={{}}
        horizontal
        pagingEnabled
        decelerationRate={0}
        snapToAlignment="center"
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        snapToInterval={blockDimensions.width + blockDimensions.margin * 2}
      >
        {data.map((el) => (
          <Block
            style={{
              marginHorizontal: blockDimensions.margin,
              paddingHorizontal: blockDimensions.padding,
              paddingVertical: blockDimensions.padding,
              paddingBottom: blockDimensions.padding + 10,
              backgroundColor: 'black',
            }}
            dimensions={blockDimensions}
          >
            <Charts.PieChart
              config={{
                radius: 120,
                strokeWidth: 10,
                max: sum(circleData, (d) => d.amount),
              }}
              data={circleData}
            />
          </Block>
        ))}
      </ScrollView>
    </View>
  )
}

///////////////////////////////////////////

export default memo(OverviewHeader)
