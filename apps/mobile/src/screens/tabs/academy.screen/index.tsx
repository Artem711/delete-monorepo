// # PLUGINS IMPORTS //
import React from 'react'
import { StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { Text } from '@mobile/components/atoms'

// # COMPONENTS IMPORTS //
import LoadingScreen from '../../simple/loading.screen'
import Card from './course-card'
import { SafeAreaView } from 'react-native-safe-area-context'

// # EXTRA IMPORTS //
import { useGetCoursesQuery } from '@lib/data-access'

/////////////////////////////////////////////////////////////////////////////

export default function AcademyScreen() {
  const { data, error, loading } = useGetCoursesQuery({
    variables: { userId: '' },
  })

  if (loading) return <LoadingScreen />
  if (error || !data?.getCourses) return <Text>Error :(</Text>

  const { getCourses: courses } = data
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        {[...courses, ...courses].map((el) => (
          <Card />
        ))}
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({})
