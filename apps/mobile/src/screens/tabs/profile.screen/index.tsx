// # PLUGINS IMPORTS //
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

// # COMPONENTS IMPORTS //
import { StoryThumbnail } from '../../simple/story.screen'

// # EXTRA IMPORTS //
import { IStory } from '../../simple/story.screen/story.screen'

/////////////////////////////////////////////////////////////////////////////

const stories: Array<IStory> = [
  {
    id: '2',
    source: require('../../../../assets/temp/stories/2.jpg'),
    user: 'derek.russel',
    avatar: require('../../../../assets/temp/avatars/derek.russel.png'),
  },
  {
    id: '4',
    source: require('../../../../assets/temp/stories/4.jpg'),
    user: 'jmitch',
    avatar: require('../../../../assets/temp/avatars/jmitch.png'),
  },
  {
    id: '7',
    source: require('../../../../assets/temp/stories/7.jpg'),
    user: 'andrea.schmidt',
    avatar: require('../../../../assets/temp/avatars/andrea.schmidt.png'),
    video: require('../../../../assets/temp/stories/7.mp4'),
  },
  {
    id: '5',
    source: require('../../../../assets/temp/stories/5.jpg'),
    user: 'monicaa',
    avatar: require('../../../../assets/temp/avatars/monicaa.png'),
  },
  {
    id: '3',
    source: require('../../../../assets/temp/stories/3.jpg'),
    user: 'alexandergarcia',
    avatar: require('../../../../assets/temp/avatars/alexandergarcia.png'),
  },
  {
    id: '1',
    source: require('../../../../assets/temp/stories/1.jpg'),
    user: 'andrea.schmidt',
    avatar: require('../../../../assets/temp/avatars/andrea.schmidt.png'),
  },
  {
    id: '6',
    source: require('../../../../assets/temp/stories/6.jpg'),
    user: 'andrea.schmidt',
    avatar: require('../../../../assets/temp/avatars/andrea.schmidt.png'),
  },
]

export const assets = stories
  .map((story) => [story.avatar, story.source])
  .flat()

export default function ProfileScreen() {
  const insets = useSafeAreaInsets()

  return (
    <ScrollView style={{ marginTop: insets.top }}>
      <View style={styles.wrapper}>
        {stories.map((story) => (
          <StoryThumbnail key={story.id} story={story} />
        ))}
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
})
