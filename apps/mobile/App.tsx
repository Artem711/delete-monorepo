import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'

// # PLUGINS IMPORTS //

// # COMPONENTS IMPORTS //
import Navigation from './src/navigation'
import { ApolloWrapper } from '@lib/client-config'

// # EXTRA IMPORTS //
import { hooks } from '@mobile/utils'

/////////////////////////////////////////////////////////////////////////////

console.disableYellowBox = true
export default function App() {
  const isLoadingComplete = hooks.useCachedResources()

  if (!isLoadingComplete) {
    return null
  } else {
    return (
      <ApolloWrapper>
        <SafeAreaProvider>
          <Navigation />
        </SafeAreaProvider>
      </ApolloWrapper>
    )
  }
}
