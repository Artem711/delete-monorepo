// # PLUGINS IMPORTS //
import React from 'react';
import Head from 'next/head';
import { AppProps } from 'next/app';

// # COMPONENTS IMPORTS //
import { ApolloWrapper } from '@lib/client-config';

// # EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

export default function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloWrapper>
      <Head>
        <title>Next JS</title>
      </Head>
      <Component {...pageProps} />
    </ApolloWrapper>
  );
}
